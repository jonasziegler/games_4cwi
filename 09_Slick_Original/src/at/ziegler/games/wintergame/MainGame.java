package at.ziegler.games.wintergame;



import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import at.ziegler.games.wintergame.Snowflakes.size;

//import com.sun.javafx.scene.traversal.Direction;

public class MainGame extends BasicGame {
	
	private List<Actor> actors;
	private List<Actor> actorsToDelete;
	private SnowmanActor snowman;
	private int snowballIntervall;
	
	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// 1mal aufgerufen
		this.actors = new ArrayList<>();
		this.actorsToDelete = new ArrayList<>();
		
		this.actors.add(new RectActor(50, 50));
		this.actors.add(new CircleActor(400,100));
		this.actors.add(new OvalActor(100, 400));
		this.actors.add(new Shootingstar());
		this.snowman = new SnowmanActor();
		this.actors.add(this.snowman);
		
		for (int i = 0; i < 30; i++) {
			this.actors.add(new Snowflakes(size.big));
			this.actors.add(new Snowflakes(size.medium));
			this.actors.add(new Snowflakes(size.small));
		}
		
		
		
	}
	
	

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf (in Millisek)
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}
		
		deleteUnusedObjects();
		
		manageSnowballs(gc, delta);
		
			
	}

	private void manageSnowballs(GameContainer gc, int delta) {
		this.snowballIntervall = this.snowballIntervall + delta;
		if(this.snowballIntervall > 200) {
			if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {
				SnowballActor sb = new SnowballActor(this.snowman.getX(), this.snowman.getY(),this);
				this.actors.add(sb);
			}
			this.snowballIntervall = 0;
		}
	}

	private void deleteUnusedObjects() {
		for (Actor actor : this.actorsToDelete) {
			//System.out.println("actor deleted");
			this.actors.remove(actor);
		}
		this.actorsToDelete.clear();
	}
	
	public void addActorToDelete(Actor actor) {
		this.actorsToDelete.add(actor);
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
