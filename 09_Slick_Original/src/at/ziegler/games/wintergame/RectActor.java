package at.ziegler.games.wintergame;



import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor implements Actor {
	private double x,y;
	private direction dir;
	private enum direction {right, down, left, up};

	public RectActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.dir = direction.right;
	}
	
	public void update(GameContainer gc, int delta) {
		
		
		if (dir==direction.right) {
			this.x++;
			
			if(x >= 700) {
				this.dir = direction.down;
			}
		}else if (dir== direction.down) {
			this.y++;
			
			if(y >= 500) {
				this.dir = direction.left;
			}
		}else if (dir == direction.left) {
			this.x--;
			
			if(x <= 100) {
				this.dir = direction.up;
			}
		}else if (dir == direction.up) {
			this.y--;
			
			if(y <=100) {
				this.dir = direction.right;
			}
		}	
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect((float)this.x, (float)this.y, 50, 50);
	}
}
