package at.ziegler.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class SnowmanActor implements Actor{
	private float x;
	private float y;
	private float width;
	private float height;
	private Image snowman;
	private float speed;
	
	
	
	public SnowmanActor() throws SlickException {
		super();
		this.x = 50;
		this.y = 450;
		this.width = 80f;
		this.height = 100f;
		this.speed = 0.5f;
		this.snowman = new Image("testdata/snowman.png");
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if (this.y < 500) {
			if(gc.getInput().isKeyDown(Input.KEY_DOWN)) {
				this.y = (this.y + delta * this.speed);
				
			}
		}	
		if(this.y > 0) {	
			if (gc.getInput().isKeyDown(Input.KEY_UP)) {
				this.y = (this.y - delta * this.speed);
			}
		}
		if(this.x < 720) {
			if (gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
				this.x = (this.x + delta * this.speed);
			}
		}
		if(this.x > 0) {
			if (gc.getInput().isKeyDown(Input.KEY_LEFT)) {
				this.x = (this.x - delta * this.speed);
			}
		}
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		this.snowman.draw(this.x, this.y, this.width, this.height);;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	
	
}
