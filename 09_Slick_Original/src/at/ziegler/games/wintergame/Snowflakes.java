package at.ziegler.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflakes implements Actor{
	
	private float speed;
	private int width;
	private int height;
	private float x;
	private float y;
	private Random rX;
	private Random rY;
	
	
	private size snowflakeSize;
	
	public enum size {
		small, medium, big
	}
	
	
	public Snowflakes(size snowflakeSize) {
		super();
		this.snowflakeSize = snowflakeSize;
		this.rX = new Random();
		this.x = rX.nextInt(780);
		this.rY = new Random();
		this.y = rY.nextInt(600) * -1;
		
		if(this.snowflakeSize == size.small) {
			this.speed = 0.2f;
			this.height = 10;
			this.width = 10;
			
		}else if(this.snowflakeSize == size.medium) {
			this.speed = 0.3f;
			this.height = 20;
			this.width = 20;
			
		}else if(this.snowflakeSize == size.big) {
			this.speed = 0.5f;
			this.height = 30;
			this.width = 30;
			
		}
	}

	public void update(GameContainer gc, int delta) {
		this.y = this.y + (this.speed * delta);
		
		if(y > 650) {
			this.x = rX.nextInt(780);
			this.y = rY.nextInt(600) * -1;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval((float)this.x, (float)this.y, this.width, this.height);
	}
}
