package at.ziegler.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor{
	private double x,y;
	private direction dir;
	private enum direction {right, left};

	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.dir = direction.right;
	}
	
	public void update(GameContainer gc, int delta) {
		if(dir == direction.right) {
			this.x++;
			if(x >=700) {
				this.dir = direction.left;
			}
			
		}else if(dir == direction.left) {
			this.x--;
			
			if(x <=100) {
				this.dir = direction.right;
			}
			
		}
	}
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 60, 40);
	}
}
