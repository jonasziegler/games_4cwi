package at.ziegler.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import java.util.Random;

import org.newdawn.slick.Color;


public class SnowballActor implements Actor {
	
	private float x;
	private float y;
	private int width;
	private MainGame mainGame;
	private Random randColor;
	private Color color;
	private int direction;
	
	
	
	public SnowballActor(float x, float y, MainGame mg) {
		super();
		this.x = x;
		this.y = y + 20;
		this.width = 20;
		this.mainGame = mg;
		this.randColor = new Random();
		this.color = new Color(java.awt.Color.HSBtoRGB(randColor.nextFloat(), 1 , 1));
	 	
		if(x > 400) {						
			this.direction = -1;
		}else {
			this.direction = 1;
		}
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
		
		if(x<800 | x>0) {
			this.x = (this.x + delta*this.direction);
			
		}else {
			this.mainGame.addActorToDelete(this);
		}
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(color);
		graphics.fillOval(this.x, this.y, this.width, this.width);
		graphics.setColor(Color.white);
	}

}
