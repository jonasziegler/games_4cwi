package at.ziegler.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Shootingstar implements Actor{
	
	private float width;
	private float x;
	private float y;
	private Random rTime;
	private float time;
	private Random rX;
	private Random rY;
	private int direction;
	private float i; 
	
	
	
	public Shootingstar() {
		super();
		newStar();
	}


	private void newStar() {
		this.rX = new Random();
		this.x = rX.nextInt(700) + 10;	
		this.rY = new Random();
		this.y = rY.nextInt(500) +10;
		this.width = 35;
		this.i = 0.4f;
		this.rTime = new Random();
		this.time = rTime.nextInt(2000);	
		
		if(x > 400) {						
			this.direction = -1;
		}else {
			this.direction = 1;
		}
	}
	

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		this.i = this.i - 0.002f;							//i ist um die fulugbahn zu aendern
		this.y = this.y - (i * delta);						//die Fulugbahn wird geaendert
		this.x = this.x + (0.5f * this.direction * delta);  //0.4f ist zum verlangsamen
		
		if(this.width > 0) {
			this.width = this.width - (0.04f * delta);		//0.04f um die geschwindigkeit der groessen aenderung zu bestimmen
			
		}else {
			this.time -= delta;		
			if(time <= 0) {			//wenn die time 0 entspricht wird der Stern an einer neuen Stelle 
				newStar();
			}
			
		}
		
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		
		graphics.setColor(Color.yellow);
		graphics.fillOval(this.x, this.y, (float)this.width, (float)this.width);
		graphics.setColor(Color.white);
	}



}
